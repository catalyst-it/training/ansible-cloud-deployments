---

#  ansible-playbook -i ansible/inventory/cloud-hosts ansible/provision-hosts.yml -
- name: Provision a set of hosts in the Catalyst Cloud
  hosts: localhost
  gather_facts: false
  vars:
    # ADD hosts to create

    # ADD extract security groups from inventory

  tasks:

    - name: Output groups variable
      debug:
        var: groups

    - name: Output host_set
      debug:
        var: host_set | mandatory

    # ADD create cloud resources

    - name: Output security groups
      debug:
        var: security_groups | mandatory

    - name: Output security group names
      debug:
        var: security_group_names | mandatory

    - name: Create security groups
      openstack.cloud.security_group:
        cloud: "{{ cloud_name | default(omit) }}"
        state: present
        name: "{{ item }}"
      loop: "{{ security_group_names }}"

    - name: Add rules for security group
      openstack.cloud.security_group_rule:
        cloud: "{{ cloud_name | default(omit) }}"
        state: present
        security_group: "{{ item.group }}"
        protocol: "{{ item.proto | default('tcp') }}"
        port_range_min: "{{ item.port }}"
        port_range_max: "{{ item.port }}"
        remote_ip_prefix: "{{ item.remote_ip_prefix | default(omit) }}"
        remote_group: "{{ item.remote_group | default(omit) }}"
      loop: "{{ security_groups }}"

    - name: Create cluster instances
      openstack.cloud.server:
        cloud: "{{ cloud_name | default(omit) }}"
        state: present
        name: "{{ item }}"
        image: "{{ default_os_image }}"
        key_name: "{{ keypair_name  }}"
        flavor: "{{ default_flavor }}"
        auto_ip: "{{ hostvars[item].assign_floating_ip | default('no') }}"
        nics:
          - net-name: "{{ network_name }}"
        security_groups: "{{ hostvars[item].security_groups | map(attribute='group') | join(',') }}"
        meta: "{{ meta_data }}"
        userdata: "{{ userdata }}"
      register: launch
      loop: "{{ host_set }}"

    - name: Append info for launched machines to host info
      add_host:
        name: "{{ item.openstack.name }}"
        ansible_host: "{{ item.openstack.public_v4 | default(item.openstack.private_v4, true) }}"
        public_v4: "{{ item.openstack.public_v4 | default(omit) }}"
        private_v4: "{{ item.openstack.private_v4 }}"
      loop: "{{ launch.results }}"

    - name: Make sure loadbalancer uses private v4
      add_host:
        name: "{{ item.openstack.name  }}"
        ansible_host: "{{ item.openstack.private_v4 }}"
      loop: "{{ launch.results }}"
      when: item.openstack.name in groups.loadbalancer

    - name: Set ssh args for bastion
      add_host:
        name: "{{ item.openstack.name  }}"
        ansible_ssh_common_args: "-o StrictHostKeyChecking=no"
      loop: "{{ launch.results }}"
      when: item.openstack.name in groups.bastion

    - name: Set ssh args for rest of cluster
      add_host:
        name: "{{ item.openstack.name  }}"
        ansible_ssh_common_args: >
          -o StrictHostKeyChecking=no
          -J {{ hostvars[item.openstack.name].ansible_user }}@{{ hostvars[groups.bastion[0]].ansible_host }}
      loop: "{{ launch.results }}"
      when: item.openstack.name in groups.private_net


    - name: Remove IP from all machines from known hosts
      known_hosts:
        name: "{{ hostvars[item.0][item.1] }}"
        state: absent
      with_nested:
        - "{{ host_set }}"
        - "{{ ['ansible_host', 'inventory_hostname'] }}"


- name: Check for connectivity to bastion
  hosts: bastion
  gather_facts: false
  tasks:
    - name: Wait for connection to be available on bastion
      wait_for_connection:
      delegate_to: localhost

- name: Add bastion public ip to known_hosts
  hosts: bastion
  gather_facts: false
  tasks:
    - block:
        - name: Perform SSH keyscan to add to known_hosts
          shell: ssh-keyscan {{ ansible_host }}
          register: bastion_ssh_key

        - name: Add ssh key to known_hosts
          known_hosts:
            name: "{{ ansible_host }}"
            key: "{{ item }}"
            state: present
          loop: "{{ bastion_ssh_key.stdout_lines }}"

        - name: Wait for bastion to be reachable over SSH
          wait_for_connection:

        - name: Output access ip for bastion
          debug:
            msg: "Access bastion at ssh -l {{ ansible_user }} {{ ansible_host }}"
      delegate_to: localhost



- name: Check connectivity to hosts
  hosts: cluster:!bastion
  gather_facts: false
  tasks:

    - name: Wait for connection to be available on hosts before proceeding
      wait_for_connection:
      delegate_to: localhost


# ADD bastion -> private_net for SSH
