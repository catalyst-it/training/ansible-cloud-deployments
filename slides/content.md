### Managing Cloud Deployments with Ansible

# Catalyst <!-- .element class="catalyst-logo" -->

Presented by [Travis Holton](#) <!-- .element: class="small-text"  -->


## Managing Cloud Deployments with Ansible <!-- .slide: class="title-slide" --> <!-- .element: class="orange" -->




## Introduction


#### About me
* <!-- .element: class="fragment" data-fragment-index="0" -->
  I work at Catalyst in Wellington
* <!-- .element: class="fragment" data-fragment-index="1" -->
  I do _devops_ related stuff
  - mostly Ansible
  - some Docker
  - some Puppet
* <!-- .element: class="fragment" data-fragment-index="2" -->
  I also do training at Catalyst
  - Ansible
  - Kubernetes


#### Where to find me
* GitHub: [github.com/heytrav](https://github.com/heytrav)
* Twitter: [@heytrav](https://twitter.com/heytrav)
* Email: travis@catalyst.net.nz


#### About this course
* Assumes some experience with Ansible
* Assumes knowledge of basic concepts:
  - variables
  - conditionals
  - iteration
  - jinja expression syntax
  - filters
  - handlers
* YAML


#### Goals of this workshop
* Examine different types of cluster deployment
* See how we can use built-in features of Ansible to manage deploying and
  upgrading
* Provide tools that can be used in any build or CI/CD pipeline


#### Course Outline
* [Setup](#setup)
* [Cloud Signup](#cloud-provider-account)
* [Review basics](#ansible-basics)
* [Provisioning Machines](#provisioning-hosts)
* [Deploying the Application](#deploying-the-application)
* [Upgrade Strategies](#upgrade-strategies)
* [In-place rolling upgrade](#in-place-rolling-upgrade)
* [Blue Green](#blue-green-deployments)
* [Closing](#the-end)


#### Source material
- Keating, Jesse. [*Mastering Ansible*](https://www.paktpub.com/au/networking-and-servers/mastering-ansbile). Packt, 2015
- Hochstein, Lorin et al. *Ansible Up & Running 2nd Edition*.
  O'Reilly, 2017
- [https://docs.ansible.com/ansible/latest/user_guide/playbooks_delegation.html](https://docs.ansible.com/ansible/latest/user_guide/playbooks_delegation.html)
- Based somewhat on my own experience



### Setup


#### Checkout the code
* Clone the course material and sample code
  ```shell
  git clone https://gitlab.com/catalyst-it/training/ansible-cloud-deployments.git
  ```
  <!-- .element: style="font-size:12pt;"  -->
* or follow along on GitHub
  - README -> [Course Outline](https://gitlab.com/catalyst-it/training/ansible-cloud-deployments#ansible-cloud-deployments)
* ..or follow along in pdf in base directory


#### Installing Ansible
* For this workshop we'll be using Ansible &ge; 2.8
* [Installation options](http://docs.ansible.com/ansible/latest/intro_installation.html)


###### Setup Python virtualenv
* Requirements
  - &ge; python3.5
  - virtualenv
* Set up local Python environment
   ```shell
   python3 -m venv ~/venv
   ```
* <!-- .element: class="fragment" data-fragment-index="0" -->Activate virtualenv as base of Python interpreter
   ```shell
   source ~/venv/bin/activate
   ```


###### Install Dependencies
* <!-- .element: class="fragment" data-fragment-index="0" -->Update Python package manager (pip)
   ```
   pip install -U pip
   ```
* <!-- .element: class="fragment" data-fragment-index="1" -->Install
  dependencies
   ```
   pip install -r requirements.txt
   ```


#### Project Layout
* Source code for exercises is under the `ansible` folder

```
ansible.cfg
ansible
├── files
│   └── rsyslog-haproxy.conf
├── group_vars
│   ├── all/
│   ├── appcluster.yml
│   ├── app.yml
│   ├── bastion.yml
│   ├── cluster/
│   ├── db.yml
│   ├── loadbalancer/
│   ├── private_net.yml
│   ├── publichosts.yml
│   └── web.yml
├── inventory
│   ├── cloud-hosts
│   └── openstack.yml
├── lb-host.yml
├── app-blue-green-upgrade.yml
├── app-rolling-upgrade.yml
├── blue-green-start-switch.yml
├── deploy.yml
├── provision-hosts.yml
├── remove-hosts.yml
├── setup-blue-green.yml
├── tasks
├── templates
│   ├── config.py.j2
```
<!-- .element: style="font-size:11pt;"  -->


#### Format for exercises
* <!-- .element: class="fragment" data-fragment-index="0" -->
  There are a few partially complete playbooks under `ansible` folder
* <!-- .element: class="fragment" data-fragment-index="1" -->
  We will complete these as we discuss important concepts
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Comments in playbooks match examples in course slides
 <pre><code class="yaml" data-trim data-noescape>
  <mark># ADD something here</mark>
  - name: Example code to add to playbook
    hosts: somehost
 </code></pre>




### Cloud Provider Account


#### Catalyst Cloud Registration
* Open [https://dashboard.cloud.catalyst.net.nz](https://dashboard.cloud.catalyst.net.nz)
* Sign up for an account


#### Download OpenStack credentials
* Select region _nz-hlz-1_
* In upper right corner under your account name![config-download](img/os-config-download.png "Download OS config") <!-- .element: class="zoom img-right" width="80%" -->
* Download _OpenStack RC File v3_


#### OpenStack SDK login
* Your terminal will need to be logged in to interact with OpenStack on the Catalyst
  Cloud
* Activate the OpenStack config in your terminal <!-- .element: class="fragment" data-fragment-index="0" -->
* This will prompt you to enter your Catalyst Cloud password <!-- .element: class="fragment" data-fragment-index="1" -->
    <pre><code data-trim data-noescape>
    source <mark>&lt;your account name&gt;</mark>.catalyst.net.nz-openrc.sh
    Please enter your OpenStack Password for project ...
    *******
    </code></pre>

Note: This will need to be done each time a new terminal is opened if
particpants want to connect via openstacksdk



### Ansible Basics
##### (quick review)


#### Terminology
<div style="width:50%;float:left;">
    <dl>
        <dt>Task      </dt> <dd>   An action to perform</dd>
        <dt>Play      </dt> <dd>   a collection of tasks</dd>
        <dt>Playbook</dt>   <dd> YAML file containing one or more plays</dd>
    </dl>
</div>

![Workflow](img/ansible-workflow.png "Ansible Workflow") <!-- .element: class="zoom" style="width:50%;float:left" -->


#### Ansible Playbook Structure
![playbook-anatomy](img/playbook-anatomy.png "Playbook anatomy") <!-- .element: style="float:left" width="50%"-->

* A playbook is a YAML file containing a list of plays
* A play is a dictionary object


#### Ansible Playbook Structure
* A play must contain:
  * `hosts`
    * A string representing a particular host or _group_ of hosts
      * `hosts: localhost`
      * `hosts: app.mywebsite.com`
      * `hosts: appserver`
    * These are what you will configure


#### Ansible Playbook Structure

* A play may optionally contain:
   * tasks
     * A list of dictionaries
     * What you want to do
   * name
     * Description of the play
   * vars
     * Variables scoped to the play


#### Structure of a Task

* A task is a dictionary object containing
  * name
    * Describes what the task does
    * Optional but best practice to use
  * module
    * Dictionary object
    * Key represents Python module which will perform tasks
    * May have arguments


#### Structure of a Task
<div style="width:50%;float:left;" class="zoom">
    <img class="zoom" src="img/ansible-task-anatomy.svg"/>
</div>
<div style="width:50%;float:left;">
    <ul>
        <li>
            Two styles of module object in tasks
            <ul>
                <li>string form</li>
                <li>dictionary form</li>
            </ul>
        </li>
        <li>
            Dictionary form is more suitable for complex arguments
        </li>
        <li>
            Matter of preference/style
        </li>
    </ul>
</div>


#### More Terminology
<dl>
                        <dt>Module    </dt> <dd>Blob of Python code which is executed to perform task</dd>
                        <dt>Inventory </dt> <dd>File containing hosts and groups of hosts to run tasks</dd>
                    </dl>


#### YAML and indentation
* <!-- .element: class="fragment" data-fragment-index="0" -->Ansible is fussy about indentation
* <!-- .element: class="fragment" data-fragment-index="1" -->TABS not allowed (Ansible will complain)
* <!-- .element: class="fragment" data-fragment-index="2" -->Playbook indentation
  <pre style="font-size:10pt;"><code data-trim data-noescape>
  - name: This is a play
  <mark>  </mark>hosts: somehosts             # 2 spaces
  <mark>  </mark>tasks:
  <mark style="background-color:lightblue">    </mark>- name: This is a task     # 4 spaces
  <mark style="background-color:lightgreen">      </mark>module:                  # 6 spaces
  <mark style="background-color:pink">        </mark>attr: someattr         # 8 spaces
  <mark style="background-color:pink">        </mark>attr1: someattr
  <mark style="background-color:pink">        </mark>attr2: someattr
</code></pre>
* <!-- .element: class="fragment" data-fragment-index="2" -->Task file indentation
  <pre style="font-size:10pt;"><code data-trim data-noescape>
  - name: This is a task
  <mark>  </mark>module: somehosts             # 2 spaces
  <mark style="background-color:lightblue">    </mark>attr1: value1               # 4 spaces
  <mark style="background-color:lightblue">    </mark>attr2: value2               # 4 spaces
  <mark style="background-color:lightblue">    </mark>attr3: value3               # 4 spaces
  <mark>  </mark>loop: "{{ list_of_items }}"   # 2 spaces
</code></pre>
* <!-- .element: class="fragment" data-fragment-index="3" -->Use an editor that supports YAML syntax


#### YAML editors
##### (in no specific order)
* [Atom](https://atom.io)
* [VisualStudio](https://code.visualstudio.com/download)
* vim
* Emacs
* Nano (?)


#### Vim YAML setup
* `.vimrc` file
  ```
  syntax on
  filetype plugin indent on
  ```
* `~/.vim/after/ftplugin/yaml.vim`
  ```
  set tabstop=2 "Indentation levels every two columns
  set expandtab "Convert all tabs that are typed to spaces
  set shiftwidth=2 "Indent/outdent by two columns
  set shiftround "Indent/outdent to nearest tabstop
  ```
  <!-- .element: style="font-size:11pt;"  -->




### Defining Infrastructure


#### Creating a cluster
* For this tutorial we are going to need 11 machines
  - 4 for nginx web server
  - 4 for our web application
  - 1 database host
  - 1 load balancer
  - 1 bastion
* Seems like a lot, but we are trying to simulate upgrades across a cluster


#### Our cluster
* <!-- .element: class="fragment" data-fragment-index="0" -->HTTP traffic reaches web hosts via load balancer
![cotd-cluster](img/cotd-bastion-arch.png "COTD cluster") <!-- .element: class="img-right"  width="30%"  -->
* <!-- .element: class="fragment" data-fragment-index="1" -->Application receives traffic from web hosts on port 5000
* <!-- .element: class="fragment" data-fragment-index="2" -->DB receives traffic from app hosts on 5432
* <!-- .element: class="fragment" data-fragment-index="3" -->SSH traffic
  - only bastion reachable from outside
  - all other hosts only SSH from bastion


#### Managing Sets of Hosts
##### static inventories
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Describe set of hosts in plain text files
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Can use different formats
  - ini
  - yaml
  - json
  - toml
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Used when cluster infrastructure does not change

<!-- .element: class="stretch"  -->


#### Ansible inventory source
* <!-- .element: class="fragment" data-fragment-index="0" -->
  By default Ansible looks in the following locations (lowest to highest
  precedence)
* <!-- .element: class="fragment" data-fragment-index="1" -->
  ```
  /etc/ansible/hosts
  ```
* <!-- .element: class="fragment" data-fragment-index="2" -->
  The `inventory` field in `ansible.cfg`
   ```ini
   [defaults]
   inventory = path/to/inventory
   ```
* <!-- .element: class="fragment" data-fragment-index="3" -->
  Argument to `-i` on the command line
  ```
  ansible-playbook -i path/to/inventory playbook.yml
  ```


#### Example Static Inventories
* <!-- .element: class="fragment" data-fragment-index="0" -->
  INI file
  ```ini
  mail.example.com

  [webservers]
  foo.example.com
  bar.example.com

  [dbservers]
  one.example.com
  two.example.com
  three.example.com
  ```
  <!-- .element: style="font-size:11pt;"  -->
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Same thing with a YAML file
  ```
  all:
    hosts:
      mail.example.com:
    children:
      webservers:
        hosts:
          foo.example.com:
          bar.example.com:
      dbservers:
        hosts:
          one.example.com:
          two.example.com:
          three.example.com:
  ```
  <!-- .element: style="font-size:11pt;"  -->

<!-- .element:  class="stretch"  -->


#### Inventories, hosts and groups
* _[groups]_ can be used to organise infrastructure in different ways
  * <!-- .element: class="fragment" data-fragment-index="0" -->
    geographical
    ```ini
    [sydney]
    web[1:2]
    app[1:2]

    [auckland]
    web[3:4]
    app[3:4]

    ```
    <!-- .element: style="font-size:8pt;"  -->
  * <!-- .element: class="fragment" data-fragment-index="1" -->
    Cluster structure
    ```ini
    [web]
    web[1:4]

    [app]
    app[1:4]

    ```
    <!-- .element: style="font-size:8pt;"  -->
* <!-- .element: class="fragment" data-fragment-index="3" -->
  Common to have multiple inventory files in one project


#### Cluster organisation
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Our static inventory
  ```
  ansible/inventory/cloud-hosts
  ```
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Visual representation ![cotd-venn](img/cotd-venn.png "COTD venn diagram") <!-- .element: class="img-right" width="45%" -->
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Translates to network security group structure of cluster


#### Inventories and variable scope
* Hosts/groups in inventory determine how variables applied in play scope
  <pre  style="font-size:8pt;"><code  data-trim data-noescape>
  [web]
  training-web[1:2]
  </code></pre>
* <!-- .element: class="fragment" data-fragment-index="0" -->
  In a play (in a playbook)
  <pre style="font-size:11pt;"><code data-trim data-noescape>
  name: Set up web servers
  hosts: <mark>web</mark>
  </code></pre>
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Variables loaded automatically
  <pre  style="font-size:8pt;"><code data-trim data-noescape>
  ansible
  ├── host_vars/                                    .
  .   .
  │   <mark>├── training-web1.yml</mark>   <-- same name as host
  .
  ├── group_vars/                                    .
  │   ├── all/
  │   ├── appcluster.yml
  .   .
  │   ├── publichosts.yml
  │   <mark>└── web.yml</mark>            <-- group variables
  </code></pre>


#### Querying static inventory
##### `ansible-inventory`
* <!-- .element: class="fragment" data-fragment-index="0" -->
  The `ansible-inventory` command can provide some info about group/host
  hierarchy
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Using *ini* or other file represents the *static* inventory
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Can see entire set of hosts
* <!-- .element: class="fragment" data-fragment-index="3" -->
  Specific information about a single host
* <!-- .element: class="fragment" data-fragment-index="4" -->
  Basic group hierarchy


#### View list of hosts
##### `--list`
* <!-- .element: class="fragment" data-fragment-index="0" -->
  `ansible-inventory --list` will output list of hosts with inventory
  variables
* <!-- .element: class="fragment" data-fragment-index="1" -->
  View list of hosts in inventory

  ```
  ansible-inventory -i inventory/cloud-hosts --list
  ```
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Note that some variables still appear in jinja2 syntax
  ```
  {
  "ansible_host": "{{ openstack.private_v4 }}",
  "ansible_ssh_host": "{{ openstack.private_v4 }}",

  }
  ```
  <!-- .element: style="font-size:11pt;"  -->


#### View info about a host
##### `--host <hostname>`
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Same info as with `--list` but for single host
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Use host name from one of hosts in `inventory/cloud-hosts`
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Try this command to view info about a specific host
  ```
  ansible-inventory -i inventory/cloud-hosts --host $PREFIX-web1
  ```
  <!-- .element: style="font-size:11pt;"  -->


#### View group hierarchy
* <!-- .element: class="fragment" data-fragment-index="0" -->
  The `--graph` argument shows group structure of inventory
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Tree-like representation in ASCII
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Can be useful to confirm your inventory is what you expect
  ```
  ansible-inventory -i inventory/cloud-hosts --graph private_net
  ```
  <!-- .element: style="font-size:11pt;"  -->
  ```
  @private_net:
    |--@appcluster:
    |  |--trainpc01-app1
    |  |--trainpc01-app2
    |  |--trainpc01-db
    |  |--trainpc01-web1
    |  |--trainpc01-web2
    |--@loadbalancer:
    |  |--trainpc01-lb
  ```
  <!-- .element: style="font-size:11pt;" class="fragment" data-fragment-index="3  -->



### Provisioning Hosts


#### The `provision-hosts.yml` playbook
```
ansible/provision-hosts.yml
```
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Tasks in the first play are executed on local machine
   <pre style="font-size:9pt;"><code data-trim data-noescape>
   name:  Provision a set of hosts in Catalyst Cloud
   hosts: <mark>localhost</mark>
   gather_facts: false
</code></pre>
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Try running the playbook:
  ```
  ansible-playbook -i inventory/cloud-hosts provision-hosts.yml
  ```
  <!-- .element: style="font-size:11pt;"  -->
  * <!-- .element: class="fragment" data-fragment-index="2" -->
    It should fail with errors
* <!-- .element: class="fragment" data-fragment-index="3" -->
  We need to add a few things for this playbook to run correctly


#### Using predefined variables
##### `groups`
* *`groups`* is a dictionary representation of the inventory file
* See output of playbook
```
"groups": {
    "all": [
        "localhost",
        "training-web1",
        "training-web2",
        "training-db",
        "training-app1",
        "training-app2",
        "training-lb",
        "training-bastion"
    ],
    "app": [
        "training-app1",
        "training-app2"
    ],
    "appcluster": [
        "training-web1",
        "training-web2",
```
<!-- .element: class="fragment" data-fragment-index="0" style="font-size:11pt;"  -->


#### Define hosts
* Need to define hosts that we plan to create
* These hosts are in *`groups.cluster`*
* Add following to `provision-hosts.yml`
  ```
  # ADD hosts to create
  host_set: "{{ groups.cluster }}"
  ```
* Re-run the playbook
* <!-- .element: class="fragment" data-fragment-index="0" -->
  This gets a little further into the playbook before failing


#### Cloud Modules
* Cloud deployments typically involve creating resources with a provider
  - Instances
  - Networks
  - Security groups, acls, etc
* We are using [OpenStack Modules](https://docs.ansible.com/ansible/latest/modules/list_of_cloud_modules.html#openstack)
  - The modules that start with `os_`


#### Setting up cloud resources
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Behind the scenes using the OpenStack API
  - same endpoints used by `openstacksdk` CLI
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Boilerplate for creating multiple cloud hosts
  - log in to cloud provider
  - create router, network, security groups
  - create each host

Note:
- all tasks with os in module are cloud api
- building cloud modules generally boilerplate


#### OpenStack cloud modules
* The first play uses cloud modules to create objects on your tenant
* Add the following tasks to `provision-hosts.yml`

```yaml
# ADD create cloud resources
- name: Connect to Catalyst Cloud
  os_auth:
    cloud: "{{ cloud_name | default(omit) }}"

- name: Create keypair
  os_keypair:
    cloud: "{{ cloud_name | default(omit) }}"
    name: "{{ keypair_name }}"
    public_key: "{{ ssh_public_key }}"

- name: Create network
  os_network:
    cloud: "{{ cloud_name | default(omit) }}"
    name: "{{ network_name }}"
    state: present
```


#### OpenStack cloud modules
```
- name: Create subnet
  os_subnet:
    cloud: "{{ cloud_name | default(omit) }}"
    name: "{{ subnet_name }}"
    network_name: "{{ network_name }}"
    state: present
    cidr: "{{ subnet_cidr }}"
    allocation_pool_start: "{{ subnet_dhcp_start }}"
    allocation_pool_end: "{{ subnet_dhcp_end }}"
    ip_version: "4"
    dns_nameservers: "{{ default_nameservers }}"

- name: Create router
  os_router:
    cloud: "{{ cloud_name | default(omit) }}"
    state: present
    name: "{{ router_name }}"
    network: "{{ public_net_name }}"
    interfaces:
      - "{{ subnet_name }}"
```
<!-- .element: style="font-size:11pt;"  -->
* After adding these tasks, run the provision playbook again


#### Set up security groups
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Running playbook still fails due to undefined variable `security_groups`
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Security groups are defined for groups of hosts under `group_vars`
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Not in the scope of the `provision-hosts.yml` playbook which runs on `localhost`
* <!-- .element: class="fragment" data-fragment-index="3" -->
  Need somehow to populate `security_groups` variable using values from group_vars


#### Predefined variables
##### `hostvars`
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Dictionary mapping all hosts to variables defined under `host_vars`
  and `group_vars` directories

* <!-- .element: class="fragment" data-fragment-index="1" -->
  Output of `ansible-inventory --list` 
  ```
  ansible-inventory -i inventory/cloud-hosts --list | jq '._meta.hostvars'
  ```
  <!-- .element: style="font-size:10pt;"  -->


#### Create security groups
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Add following to playbook
  ```
  # ADD extract security groups from inventory
  security_groups: "{{ host_set | map('extract', hostvars, 'security_groups') | sum(start=[]) | list | unique }}"
  security_group_names: "{{ security_groups | map(attribute='group') | list | unique }}"
  ```
  <!-- .element: style="font-size:9pt;"  -->
* <!-- .element: class="fragment" data-fragment-index="1" -->
  This uses the *map* filter in two different ways
  - <!-- .element: class="fragment" data-fragment-index="2" -->
    *Extract* `security_groups` from each host and combine into list
  - <!-- .element: class="fragment" data-fragment-index="3" -->
    Grab field from each item in `security_groups` to generate
    `security_group_names`

<!-- .element: class="stretch"  -->


#### Resolving for SSH access
* We need to set up bastion to resolve other hosts
  ```
  /etc/hosts
  ```
  <!-- .element: style="font-size:12pt;"  -->
  ```
  192.168.99.100  training-lb
  192.168.99.101  training-web1
  192.168.99.102  training-web2
  .
  .
  ```
  <!-- .element: style="font-size:10pt;"  -->
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Add following to `provision-hosts.yml`
  ```yaml
  # ADD bastion -> private_net for SSH
  - name: Set up the bastion host mapping
    hosts: bastion
    become: true
    tasks:
      - name: Add entry to /etc/hosts for all instances
        lineinfile:
          dest: /etc/hosts
          line: "{{ hostvars[item].ansible_host }} {{ item }}"
        loop: "{{ groups.private_net }}"
  ```
  <!-- .element: style="font-size:10pt;"  -->


#### Provision Hosts
*  Run the provision playbook again
   ```
   ansible-playbook -i inventory/cloud-hosts provision-hosts.yml
   ```
   <!-- .element: style="font-size:11pt;" class="stretch"  -->

* Note output of `security_groups` and `security_group_names`
* Should take a few minutes to set up cluster
* In case task fails with SSH error just hit `CTRL-C` and restart
<!-- .element: class="stretch"  -->



### Dynamic Inventories


#### Dynamic Inventories
* Executable scripts that produce json output
* Query cloud API/Database to fetch info about infrastructure
* Dynamic inventories useful when hosts managed outside of Ansible
  - Cloud provider autoscaling groups
  - Provisioning tools (eg. terraform)
* Reduce human error by automating control of inventory


#### Inventory Plugins
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Dynamic inventories for some providers integrated into Ansible
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Enable [plugins](https://docs.ansible.com/ansible/latest/plugins/inventory.html#plugin-list) in `ansible.cfg`
  ```ini
  [inventory]
  enable_plugins = ini, yaml, script, openstack
  ```
  - <!-- .element: class="fragment" data-fragment-index="2" -->
    This tells Ansible to use
    - <!-- .element: class="fragment" data-fragment-index="3" -->
      ini file
    - <!-- .element: class="fragment" data-fragment-index="4" -->
      yaml file
    - <!-- .element: class="fragment" data-fragment-index="5" -->
      executable script
    - <!-- .element: class="fragment" data-fragment-index="6" -->
      openstack plugin
  


#### Configuring Openstack Inventory
* The `openstack` plugin looks for a file called `openstack.yml`
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Configuration creates pseudo groups based on attributes or special
  conditionals
  ```yaml
  plugin: openstack

  keyed_groups:
    - key: openstack.metadata.host_prefix
      separator: ""
      prefix: ""

  groups:
    appcluster: openstack.metadata.host_prefix == 'trainpc02' and 'appcluster' in openstack.metadata.pre_groups.split(',')
    cluster: openstack.metadata.host_prefix == 'trainpc02' and 'cluster' in openstack.metadata.pre_groups.split(',')
    loadbalancer: openstack.metadata.host_prefix == 'trainpc02' and 'loadbalancer' in openstack.metadata.pre_groups.split(',')
    .
    .
  ```
  <!-- .element: style="font-size:8pt;" class="stretch"  -->
* <!-- .element: class="fragment" data-fragment-index="1" -->
  This set up is a workaround to make inventory work on shared tenant


#### Dynamic inventories
##### `ansible-inventory`
* <!-- .element: class="fragment" data-fragment-index="0" -->
  As with static inventories can use dynamic inventories to view info about hosts
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Cloud provider plugins add additional information
  - Additional groups based on region, meta info
  - Attributes for virtual hosts
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Usage same as with static inventory
  ```
  ansible-inventory -i inventory/openstack.yml --list
  ```
  <!-- .element: style="font-size:11pt;"  -->
  ```
  ansible-inventory -i inventory/openstack.yml --host trainpc01-web1
  ```
  <!-- .element: style="font-size:11pt;"  -->


##### Openstack Inventory Plugin
* The `openstack` plugin adds an additional dictionary with info about host
  ```
  "trainpc01-web1": {
    "openstack": {
      .
      .
    }
  }

  ```
* This dictionary contains IP addresses needed to set up
  networking
  - public_v4
  - private_v4


##### Exercise: Find public ip
* Find the public IP address for hosts in cluster
  - bastion
  - loadbalancer
  - others?
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Example:
  ```
  ansible-inventory -i inventory/openstack.yml --host $PREFIX-bastion | jq '.openstack.public_v4'
  ```
  <!-- .element: style="font-size:10pt;"  -->



### Cluster Networking


#### Accessing hosts in the cluster
* Hosts in *private_net* group do not have public IP ![cotd-venn-private](img/cotd-venn-private_net.png "Private net") <!-- .element: class="img-right" width="60%" -->
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Only bastion is directly accessible by SSH
* <!-- .element: class="fragment" data-fragment-index="2" -->
  All other hosts can only be reached from `bastion`


#### Accessing hosts in cluster
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Security groups only allow access via bastion
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Earlier you found public IP of bastion host
  ```shell
  ansible-inventory --host training-bastion | jq '{"publicIP": .openstack.public_v4}'
  ```
  <!-- .element: style="font-size:11pt;"  -->
  ```json
  {
    "publicIP": "<your public ip>"
  }
  ```
  <!-- .element: style="font-size:11pt;" class="fragment" data-fragment-index="2"  -->
* <!-- .element: class="fragment" data-fragment-index="3" -->
  Use this to SSH into cluster
  ```shell
  ssh -t ubuntu@<your public ip> ssh <your host>
  ```


#### Traversing a bastion with Ansible
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Ansible must also access hosts in `private_net` group via
  bastion
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Need to set up SSH arguments for hosts in `private_net` group
  ```
  group_vars/private_net.yml
  ```
  <!-- .element: class="fragment" data-fragment-index="1" -->
  ```
  # ADD SSH proxy pass config in private_net
  ansible_ssh_common_args: >
    -o StrictHostKeyChecking=no 
    -J {{ ansible_user }}@{{ hostvars[groups.bastion[0]].ansible_host }}

  ansible_host: "{{ openstack.private_v4 }}"
  ansible_ssh_host: "{{ openstack.private_v4 }}"
  ```
<!-- .element:  class="stretch fragment" style="font-size:11pt;"  data-fragment-index="2" -->


#### Additional setup for hosts
* Earlier we added mapping for bastion to each host
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Need to do the same for other hosts
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Edit `/etc/hosts` on each host
  * _web_ to resolve _app_ host for nginx
  * _app_ host to resolve _db_ for database access
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Set NZ locale, timezone, etc.


#### Resolving application services
* Add following to 
  ```shell
  ansible/setup-networking.yml
  ```
  ```
  # ADD web -> app for proxy pass
  - name: Set up web hosts with mapping to backend
    hosts: web
    become: true
    tasks:

      - name: Map each frontend host to speak to a specific backend
        lineinfile:
          dest: /etc/hosts
          line: "{{ hostvars[groups.app[(group_index | int) - 1]].openstack.private_v4 }} backend"

  # ADD app -> db for application
  - name: Add mapping for db on app boxes
    hosts: app
    become: true
    tasks:

      - name: Map each app host to speak to db
        lineinfile:
          dest: /etc/hosts
          line: "{{ hostvars[item].openstack.private_v4 }} {{ item }}"
        loop: "{{ groups.db }}"
  ```
  <!-- .element: style="font-size:8pt;"  -->
* Note that we use data from dynamic inventory to find IPs


#### Set up locale and timezone
* Add following to 
  ```shell
  ansible/setup-networking.yml
  ```
  ```
  # ADD locale and timezone
  - name: Set locale and local timezone
    hosts: cluster
    become: true
    tasks:

      - name: Add NZ locale to all instances
        locale_gen:
          name: en_NZ.UTF-8
          state: present

      - name: Set local timezone
        timezone:
          name: Pacific/Auckland
  ```
  <!-- .element: style="font-size:10pt;"  -->

<!-- .element: class="stretch"  -->


#### Set up networking
* Run the networking playbook
  ```
  ansible-playbook setup-networking.yml
  ```



### Deploying the application


#### Deploying our application
* The `deploy.yml` playbook sets up our application ![Basic app](img/simple-project-app.svg "cotd application") <!-- .element: class="img-right" width="50%"-->
  * Web server running nginx
  * App server running a Python Flask
  * Postgresql Database
  * HA proxy


#### Overview of deploy playbook
* `--list-tasks <playbook>` gives an overview of plays and
  tasks
  ```
  ansible-playbook deploy.yml --list-tasks
  ```
  ```
  play #1 (private_net): Set ansible_host for private hosts     TAGS: []                                                                                  [0/19740]
    tasks:
    .
  play #2 (cluster): Update apt cache on all machines   TAGS: []
    tasks:
    .

  play #3 (db): Set up database machine TAGS: [deploy,db]
    tasks:
    .

  play #4 (db): Set up app and database machine TAGS: [deploy,db]
    tasks:
    .

  play #5 (app): Set up app server      TAGS: [deploy,app]
    tasks:
    .

  play #6 (web): Set up nginx on web server     TAGS: [deploy,web]
    tasks:
    .
  ```
  <!-- .element: style="font-size:8pt;"  -->


#### Role of Inventory and Groups
* _hosts_ attribute influences which hosts Ansible interacts with
  <pre><code data-trim data-noescape>
  hosts: <mark>web</mark>
  </code></pre>
* This will interact with all hosts in the _web_ group ![web-group](img/cotd-venn-web.png "Web group") <!-- .element: class="img-right" width="45%" -->


#### Role of Inventory and Groups
  <pre><code data-trim data-noescape>
  hosts: <mark>app</mark>
  </code></pre>
* This will interact with all hosts in the _app_ group ![app-group](img/cotd-venn-app.png "App group") <!-- .element: class="img-right" width="45%" -->


#### Set up the application
* <!-- .element: class="fragment" data-fragment-index="0" -->
  The application play sets up our app
  ```
  - name: Set up app server
    host: app
  ```
  - updates python libs
  - checks out app from GitHub
  - configures app
  - queues handler if anything changes


#### Import the setup tasks
* <!-- .element: class="fragment" data-fragment-index="0" -->
  We need to perform the same tasks for deploying and later updating the
  application
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Therefore easier to manage tasks in a separate file
  ```
  ansible/tasks/setup-app.yml
  ```
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Import these into the `deploy.yml` playbook
  ```
  # ADD import tasks
  - import_tasks: tasks/setup-app.yml
    vars:
      deploy_version: "{{ app_version }}"
      
  ```


#### Deploying the application
* Run the deploy playbook
  ```shell
  ansible-playbook deploy.yml
  ```
* Once deploy is finished it should output url of application
* Should be able to open in your browser as:
  ```
  http://<public ip>.xip.io/
  ```


#### Viewing HAProxy stats
* HAProxy provides an overview of active web hosts in cluster
  ```
  http://<public ip>.xip.io/haproxy?stats
  ```
* Login details
  - user: admin
  - password: train



### Upgrade strategies


####  What can go wrong?
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Without some kind of redundancy, we risk disrupting entire operation 

<div  class="fragment" data-fragment-index="0">

![update all at once](img/upgrade-complete-outage.svg "All at once upgrade")
<!-- .element width="50%" height="50%"-->
</div>


#### Ideal upgrade scenario
* Minimal or zero downtime during upgrade of application
* Do not deploy a broken version of our application


#### Upgrade Strategies
* In-place rolling upgrade
* Blue-Green


#### In-place rolling upgrade

* <!-- .element: class="fragment" data-fragment-index="0" -->
  Traditional approach to upgrading applications across a cluster
  - Creating new infrastructure can be prohibitively expensive
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Operates on infrastructure that already exists
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Minimise downtime by upgrading parts of the cluster at a time


#### Upgrading applications
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Why not just run the deploy playbook?
 <pre style="font-size:12pt;"><code data-trim data-noescape> 
  ansible-playbook deploy.yml <mark>-e app_version=v2 --limit app</mark>
 </code></pre>
 - <!-- .element: class="fragment" data-fragment-index="1" -->
   Run deploy playbook with additional options
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Tempting to just rely on idempotent behaviour to _do the right thing_
* <!-- .element: class="fragment" data-fragment-index="3" -->
  There are two problems with this approach
  - Playbook does not verify application actually _works_
  - Ansible's default _batch management_ behaviour


#### Deploying broken code
* Broken code may not be obvious in task
* <!-- .element: class="fragment" data-fragment-index="2" -->
  <code style="color:red;">*</code>One task leaves application in a broken state
* <!-- .element: class="fragment" data-fragment-index="4" -->
  Run the deploy playbook with `app_version=v3`

|Tasks | Host1 | Host2 |
|---   | ---   | ---   |
|task1 |  <code style="color:green;" class="fragment" data-fragment-index="0">ok</code>     |   <code style="color:green;" class="fragment" data-fragment-index="0">ok</code>    |
|task2 |  <code style="color:green;" class="fragment" data-fragment-index="1">ok</code>     |    <code style="color:green;" class="fragment" data-fragment-index="1">ok</code>   |
|task3<code style="color:red;" class="fragment" data-fragment-index="2">\*</code>  |   <code style="color:green;" class="fragment" data-fragment-index="2">ok</code>    |   <code style="color:green;" class="fragment" data-fragment-index="2">ok</code>    |
|task4 |   <code style="color:green;" class="fragment" data-fragment-index="3">ok</code>    |   <code style="color:green;" class="fragment" data-fragment-index="3">ok</code>    |

<!-- .element: style="float:left" width:50%  -->

![broken-app](img/rolling-upgrade-complete-outage.svg "Broken upgrade") <!-- .element: class="fragment" width="50%" data-fragment-index="4"  -->


#### Default _batch management_ behaviour
* By default runs each task on all hosts concurrently
* <!-- .element: class="fragment" data-fragment-index="4" -->A failed task might leave every host in cluster in a broken state

|Tasks | Host1 | Host2 |
|---   | ---   | ---   |
|task1 |  <code style="color:green;" class="fragment" data-fragment-index="0">ok</code>     |   <code style="color:green;" class="fragment" data-fragment-index="0">ok</code>    |
|task2 |  <code style="color:green;" class="fragment" data-fragment-index="1">ok</code>     |    <code style="color:green;" class="fragment" data-fragment-index="1">ok</code>   |
|task3 |   <code style="color:red;" class="fragment" data-fragment-index="2">fail</code>    |   <code style="color:red;" class="fragment" data-fragment-index="2">fail</code>    |
|task4  |    <code class="fragment" data-fragment-index="3">-</code>   |   <code class="fragment" data-fragment-index="3">-</code>    |

```
TASK [Install app requirements] ******************************
Tuesday 20 August 2019  07:44:32 +1200 (0:00:02.565)   0:00:09
changed: [training-app1]
changed: [training-app2]

TASK [Install gunicorn] ******************************
Tuesday 20 August 2019  07:44:32 +1200 (0:00:02.565)   0:00:09
failed: [training-app1]
failed: [training-app2]
```
<!-- .element: class="fragment" data-fragment-index="2" style="font-size:8pt;"  -->


#### Fixing the batch problem
##### `serial`
* <!-- .element: class="fragment" data-fragment-index="0" -->
  The `serial` attribute can be added to the _play_ attributes
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Determines batch size Ansible will operate in parallel
  - <!-- .element: class="fragment" data-fragment-index="2" -->
    integer
    * <code>serial: 1</code>
    * <code>serial: 3</code>
  - <!-- .element: class="fragment" data-fragment-index="3" -->
    percentage of cluster
    * <code>serial: 50%</code>


#### Controlled batch size
* Running with `serial` attribute set to 1

|Tasks | Host1 | Host2 |
|---   | ---   | ---   |
|task1 |  <code style="color:green;" class="fragment" data-fragment-index="0">ok</code>     |   <code style="color:green;" class="fragment" data-fragment-index="4">ok</code>    |
|task2 |  <code style="color:green;" class="fragment" data-fragment-index="1">ok</code>     |    <code style="color:green;" class="fragment" data-fragment-index="5">ok</code>   |
|task3  |   <code style="color:green;" class="fragment" data-fragment-index="2">ok</code>    |   <code style="color:green;" class="fragment" data-fragment-index="6">ok</code>    |
|task4 |   <code style="color:green;" class="fragment" data-fragment-index="3">ok</code>    |   <code style="color:green;" class="fragment" data-fragment-index="7">ok</code>    |


#### `app-rolling-upgrade`
* <!-- .element: class="fragment" data-fragment-index="0" -->
  For the following exercises we'll be using
  ```
  ansible/app-rolling-upgrade.yml
  ```
* <!-- .element: class="fragment" data-fragment-index="1" -->
  First add the tasks we need to update the application
  ```
  # ADD import tasks
  - import_tasks: tasks/setup-app.yml
    vars:
      deploy_version: "{{ app_version }}"
      
  ```
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Run playbook with `v1`
  ```
  ansible-playbook -e app_version=v1 app-rolling-upgrade.yml
  ```
  <!-- .element: style="font-size:10pt;"  -->


#### Using `serial` in our upgrade
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Update `app-rolling-upgrade.yml` as follows:
  <pre style="font-size:10pt;"><code data-trim data-noescape class="yaml">
  - name: Upgrade application in place
    become: true
    hosts: app
    # Serial attribute
    <mark>serial: 1</mark>
  </code></pre>
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Run the `app-rolling-upgrade.yml` playbook again with `-e app_version=v2`
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Try again with `-e app_version=v3`


#### Deploying broken code
* Deploying `app_version=v3` still breaks the application

|Tasks | Host1 | Host2 |
|---   | ---   | ---   |
|task1 |  <code style="color:green;" class="fragment" data-fragment-index="0">ok</code>     |   <code style="color:green;" class="fragment" data-fragment-index="4">ok</code>    |
|task2 |  <code style="color:green;" class="fragment" data-fragment-index="1">ok</code>     |    <code style="color:green;" class="fragment" data-fragment-index="5">ok</code>   |
|task3<code style="color:red;" class="fragment" data-fragment-index="2">\*</code>  |   <code style="color:green;" class="fragment" data-fragment-index="2">ok</code>    |   <code style="color:green;" class="fragment" data-fragment-index="6">ok</code>    |
|task4 |   <code style="color:green;" class="fragment" data-fragment-index="3">ok</code>    |   <code style="color:green;" class="fragment" data-fragment-index="7">ok</code>    |


#### Reset the environment
* Before we proceed, please reset your environment
  ```
  ansible-playbook app-rolling-upgrade.yml -e app_version=v1
  ```
  <!-- .element: style="font-size:12pt;"  -->


#### Failing fast
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Need to detect broken application and stop deployment
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Verify app is running after upgrade
* <!-- .element: class="fragment" data-fragment-index="2" -->
  The Flask web application that runs on app server listens on port 5000
* <!-- .element: class="fragment" data-fragment-index="3" -->
  Can use `wait_for` to stop and listen for port to be open before proceeding


#### Listen on port
* Add following to `app-rolling-upgrade.yml`
  ```
  # ADD wait for 5000
  - name: Make sure gunicorn is accepting connections
    wait_for:
      port: 5000
      timeout: 60
      delay: 20
  ```
* We're still missing something so don't run the playbook yet!


#### Flushing handlers
* The application may not have loaded new configuration
* We need to force handler to restart gunicorn before waiting on port
* Add following to `app-rolling-upgrade.yml`
  ```
  # ADD flush handlers
  - meta: flush_handlers
  ```
* Now re-run the playbook with `-e app_version=v3`


#### Failing fast
* Playbook stops execution on first host when check on port fails

|Tasks | Host1 | Host2 |
|---   | ---   | ---   |
|task1 |  <code style="color:green;" class="fragment" data-fragment-index="0">ok</code>     |   <code class="fragment" data-fragment-index="4">-</code>    |
|task2 |  <code style="color:green;" class="fragment" data-fragment-index="1">ok</code>     |    <code class="fragment" data-fragment-index="4">-</code>   |
|restart gunicorn<code style="color:red;" class="fragment" data-fragment-index="2">\*</code>  |   <code style="color:green;" class="fragment" data-fragment-index="2">ok</code>    |   <code class="fragment" data-fragment-index="4">-</code>    |
|wait_for |   <code style="color:red;" class="fragment" data-fragment-index="3">fail</code>    |   <code style="color:green;" class="fragment" data-fragment-index="4">-</code>    |


#### Load balancing and upgrades
* <!-- .element: class="fragment" data-fragment-index="0" -->
  During an upgrade we change configuration and restart the application
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Downtime might be disruptive to users of website
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Following update with `app_version=v3` half of the cluster is broken
  ```
  curl --head http://<public ip>.xip.io
  ```
  ```
  HTTP/1.1 502 Bad Gateway
  ```


#### Reset the environment
* Before we proceed, please reset your environment
  ```
  ansible-playbook app-rolling-upgrade.yml -e app_version=v1
  ```
  <!-- .element: style="font-size:12pt;"  -->


#### Avoiding disruptions
* Ideally the loadbalancer should not send traffic to the host(s) we are
  updating
* While upgrading _app_ host, need to disable traffic to upstream web host


#### Host Context
* <!-- .element: class="fragment" data-fragment-index="0" -->
  The `hosts:` attribute of a play determines *context*
  <pre><code data-trim data-noescape>
  - name: Play on app host
    <mark>hosts: app</mark>
  </code></pre>
* <!-- .element: class="fragment" data-fragment-index="1" -->
  While on host `app1`, we can call all inventory variables by name, i.e.
  - `ansible_host`
* <!-- .element: class="fragment" data-fragment-index="2" -->
  If we want variable for a different host, must use _hostvars_
  - hostvars['otherhost'].ansible_host


#### Delegation
##### `delegate_to`
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Sometimes need to configure one host *in the context of another host*
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Use `delegate_to` directive ![Delegation](img/delegation-concept.svg "Delegation") <!-- .element: class="img-right" width="30%" -->
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Run a command on server **B** using inventory variables or _facts_ from **A**


#### Using delegation
* We want to disable host we're updating on the loadbalancer
  <pre class="fragment" data-fragment-index="0"><code data-trim data-noescape>
    # ADD disable application at lb
    - name: Disable application at load balancer
      haproxy:
        backend: catapp-backend
        host: "{{ web_server }}"
        state: disabled
      <mark>delegate_to: "{{ item }}"</mark>
      <mark>loop: "{{ groups.loadbalancer }}"</mark>
  </code></pre>


#### Enabling host at loadbalancer
* When we are sure the app is running, we need to re-enable traffic to the
  host
  <pre class="fragment" data-fragment-index="0"><code data-trim data-noescape>
    # ADD enable application at lb
    - name: Re-enable application at load balancer
      haproxy:
        backend: catapp-backend
        host: "{{ web_server }}"
        state: enabled
      <mark>delegate_to: "{{ item }}"</mark>
      <mark>loop: "{{ groups.loadbalancer }}"</mark>
  </code></pre>


#### In Place Rolling Upgrade
* Run playbook with `app_version=`
  - v1
  - v2
  - v3
* During upgrades
  - curl site url from terminal
  - check HAProxy stats
* Upgrade  to v3 should not leave entire cluster (or part of site) broken


#### First step of in place upgrade

![step2](img/rolling-upgrade-phase1.svg "Upgrade first cluster") <!-- .element
width="50%" height="50%"-->

* <!-- .element: class="fragment" data-fragment-index="0" -->
  Disable application at LB (no HTTP requests)
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Upgrade necessary applications, configuration
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Re-enable at LB


#### In place rolling upgrade
![step3](img/rolling-upgrade-phase2.svg "Upgrade other clusters") <!-- .element width="50%" height="50%"-->

* <!-- .element: class="fragment" data-fragment-index="0" -->
  Repeat process across pool
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Mixed versions will be running for a period of time



### Blue Green Deployments


#### Blue Green Deployments
![start](img/blue-green-state0.png "Blue Green Start")

Start with all traffic going to _blue_ hosts


#### Begin update
![blue-green-step1](img/blue-green-state1.png "Blue Green begin upgrade")

Update traffic on _green_ hosts


#### Check green is ok
![blue-green-step2](img/blue-green-state2-ok.png "Blue green green ok")

Direct traffic through green and verify ok


#### Update blue side of cluster
![blue-green-step3](img/blue-green-state3.png "Update blue side")

Ok, so update _blue_ side


#### Reset traffic to blue
![blue-green-step4](img/blue-green-state4.png "Reset traffic to blue")

Reset traffic to _blue_ side of cluster


#### Dealing with failure
![blue-green-step2-fail](img/blue-green-state2-fail.png "Green fails")

Alternative if green app not healthy


#### Return to original state
![blue-green-original](img/blue-green-state1.png "Return to original state")

Redirect traffic back to _blue_


#### Setting up Blue-Green
* <!-- .element: class="fragment" data-fragment-index="0" --> 
  Add to `setup-blue-green.yml`
  ```
  # ADD Task to add hosts to 'ad hoc' group
  - name: Add active hosts to group
    add_host:
      name: "{{ item }}"
      groups:
        - active
    loop: "{{ active | default(groups.blue_green) }}"
  ```
  <!-- .element: style="font-size:10pt;"  -->
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Add variable to store active hosts
  ```
  # ADD variable to store set of 'live' hosts
  active: "{{ groups[ live | default('blue_green') ] }}"
  ```



#### Setting up Blue-Green
* We need to put our cluster in _blue-green_ mode
* <!-- .element: class="fragment" data-fragment-index="0" -->
  First reset our environment
  ```
  ansible-playbook app-rolling-upgrade.yml -e app_version=v1
  ```
  <!-- .element: style="font-size:11pt;"  -->
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Run playbook to switch to blue-green mode:
  ```
  ansible-playbook setup-blue-green.yml -e live=blue
  ```
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Verify half of cluster active on HAProxy stats page


#### Blue green update playbook
* For blue green we will use the following playbook
  ```
  app-blue-green-upgrade.yml
  ```
* In our inventory
  ```
  # ansible/inventory/cloud-hosts
  [blue]
  training-web1
  training-app1

  [green]
  training-web2
  training-app2

  [blue_green:children]
  blue
  green
  ```
  <!-- .element: style="font-size:11pt;"  -->


#### Inventory and groups <!-- .slide: class="image-slide" -->

![web-app-blue-green](img/web-app-blue-green.png "Web and app and blue
green groups")


#### Ad hoc groups
* For _blue-green_ we need to assign _active_ and _not active_ half of cluster
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Can assign groups of hosts to _ad hoc_ groups
* <!-- .element: class="fragment" data-fragment-index="1" -->
  By default we declare _blue_ active
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Add following to `app-blue-green-upgrade.yml`
   <pre style="font-size:8pt;" ><code data-trim data-noescape>
   # ADD set active group
  - name: Set live group as active
    hosts: localhost
    gather_facts: false
    vars:
      active: "{{ groups[ live | default('blue') ] }}"
    tasks:
      - name: Add active hosts to group
        add_host:
          name: "{{ item }}"
          groups:
        <mark>    - active</mark>
        loop: "{{ active | default(groups.blue_green) }}"
</code></pre>


#### Operating on groups and subgroups
* The play we added creates an  _ad hoc_ group called **active**
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Initially equal to **blue** group
  ![cotd-blue-active](img/cotd-blue-green-venn-active.png "Blue Active") <!-- .element: class="img-right" -->
* <!-- .element: class="fragment" data-fragment-index="1" -->
  We want to update hosts **not in the active** group



### Set Operators


#### Ansible set theory operators
* The _hosts_ attribute has syntax for set theory operations on inventory
* These enable fine control over which hosts playbooks operate


#### Union
##### A ∪ B

Combination of hosts in two groups

![union](img/union.svg "Union") <!-- .element: width="20%" height="20%" -->

All hosts in<!-- .element: class="fragment" data-fragment-index="0" --> _web_ and _db_ groups

<pre  class="fragment" data-fragment-index="0"><code data-trim data-noescape>
- name: Union of hosts
  <mark>hosts: web:db</mark>
  tasks:
</code></pre>


#### Intersection
##### A ∩ B

Hosts that are in first and second group

![Intersect](img/intersect.svg "Intersection") <!-- .element: width="20%"
height="20%" -->

<!-- .element: class="fragment" data-fragment-index="0" -->
Hosts that are in both the _web_ and the _blue_ group

<pre  class="fragment" data-fragment-index="0"><code data-trim data-noescape>
- name: Intersection of hosts
  <mark>hosts: web:&blue</mark>
  tasks:
</code></pre>


#### Difference
##### A \ B

Set of hosts in first set but not in second set

![Difference](img/difference.svg "Difference")<!-- .element: width="20%"
height="20%" -->

Hosts that are in the<!-- .element: class="fragment" data-fragment-index="0" --> _app_ group **but not** in the _active_ group

<pre  class="fragment" data-fragment-index="0"><code data-trim data-noescape>
- name: Difference of groups
  <mark>hosts: app:!active</mark>
  tasks:
</code></pre>


#### Set operators and upgrade
* Update playbook similar to rolling upgrade example
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Update app in inactive part of cluster
  ```
  # ADD set to update
  hosts: app:!active
  ```
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Should update app2 ![venn-blue-green-start](img/cotd-venn-blue-green-start.png "Blue green start") <!-- .element: class="img-right" width="50%"-->


#### Verify app is running
*  Restart the app and verify it is listening on port
   ```
   # ADD flush handlers and check port
   - meta: flush_handlers

   - name: Make sure gunicorn is accepting connections
     wait_for:
       port: 5000
       timeout: 60
       delay: 20
   ```


#### Enabling traffic to green
* Use delegation to enable traffic to green at loadbalancer
  ```
  # ADD enable traffic to inactive
  - name: Enable traffic to updated app server
    hosts: web:!active
    become: true
    tasks:
      - name: Enable application at load balancer
        haproxy:
          backend: catapp-backend
          host: "{{ inventory_hostname }}"
          state: enabled
        delegate_to: "{{ item }}"
        loop: "{{ groups.loadbalancer }}"
  ```


#### Stop traffic to blue
* Now disable blue side at loadbalancer
  ```
  # ADD disable traffic to active side
  - name: Stop traffic to initial live group
    hosts: web:&active
    become: true
    tasks:
      - name: Disable application at load balancer
        haproxy:
          backend: catapp-backend
          host: "{{ inventory_hostname }}"
          state: disabled
        delegate_to: "{{ item }}"
        loop: "{{ groups.loadbalancer }}"
  ```

<!-- .element: class="stretch"  -->


#### Run blue green upgrade
* Let's run the blue green upgrade playbook
  ```
  ansible-playbook app-blue-green-upgrade.yml -e app_version=v2
  ```
  <!-- .element: style="font-size:11pt;"  -->
* Can switch back to blue active by running
  ```
  ansible-playbook setup-blue-green.yml -e live=blue
  ```
  <!-- .element: style="font-size:11pt;"  -->
* Try running upgrade with v3 and v4


#### Additional check
* <!-- .element: class="fragment" data-fragment-index="0" -->May want to make additional checks on site
* <!-- .element: class="fragment" data-fragment-index="1" -->v4 works but does not display version on site
* Add <!-- .element: class="fragment" data-fragment-index="2" -->additional check to play
  ```
  # ADD check version display
  - name: Check that the site is reachable via nginx
    uri:
      url: "http://{{ ansible_host }}:5000"
      status_code: 200
      return_content: yes
      headers:
        HOST: "{{ hostvars[groups.loadbalancer[0]].openstack.public_v4 }}.xip.io"
    register: app_site
    failed_when: "'version: ' + app_version not in app_site.content"
    delegate_to: "{{ web_server }}"
  ```
  <!-- .element: style="font-size:10pt;"  -->



### Recovering from failure


#### Rolling back
* <!-- .element: class="fragment" data-fragment-index="0" -->
  The _rolling upgrade_ scenario prevented us from deploying a broken site.
* <!-- .element: class="fragment" data-fragment-index="1" -->
  It also prevented our site from going down
* <!-- .element: class="fragment" data-fragment-index="2" -->
  However, it also left part of our cluster offline and broken
* <!-- .element: class="fragment" data-fragment-index="3" -->
  Might want to return things to original state


#### Catching errors
##### `block-rescue-always`
* Similar to `try-catch` in most programming languages
  ```
  - block:
    - name: Inside block
      debug:
        msg: "Inside a block"

    - name: Task that fails
      fail:
        msg: "I fail"

    - name: Task that never executes
      debug:
        msg: "I never run"
   rescue:
    - name: Task jumps to here
      debug:
        msg: "I run instead"
   always:
    - name: Always runs
      debug:
        msg: "I always run"

  ```
  <!-- .element: style="font-size:8pt;"  -->
* <!-- .element: class="fragment" data-fragment-index="0" -->
  `block` is at same indentation as a normal task
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Tasks in block/rescue need to be indented 


#### Reset everything
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Reactivate both sides of cluster
  ```
  ansible-playbook setup-blue-green.yml -e live=blue_green
  ```
  <!-- .element: style="font-size:11pt;"  -->
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Set our environment back to v1
  ```
  ansible-playbook app-rolling-upgrade.yml -e app_version=v1
  ```
  <!-- .element: style="font-size:11pt;"  -->


#### Get deployed version
* <!-- .element: class="fragment" data-fragment-index="0" -->
  In order to revert we need the version to revert back to
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Grab the version that is installed using git
  ```
  # ADD get current version
  - name: Register current version of application
    command: git describe --tags
    args:
      chdir: "{{ app_directory }}"
    register: git_describe_output
    failed_when:
      - git_describe_output.rc != 0
      - not git_describe_output.stderr is search('not a git repository')
  ```
  <!-- .element: style="font-size:10pt;"  -->
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Add variable to catch the current version from stdout
  ```
  # ADD pre upgrade vars
  vars:
    pre_upgrade_version: "{{ git_describe_output.stdout }}"
  ```
  <!-- .element: style="font-size:10pt;"  -->


#### Implementing recovery
* Modify the `app-rolling-upgrade.yml` playbook
* Move upgrade section into a block
  ```
    - block:
        # ADD import tasks
        - import_tasks: tasks/setup-app.yml
          vars:
            deploy_version: "{{ app_version }}"

        # ADD flush handlers
        - meta: flush_handlers

        # ADD wait for 5000
        - name: Make sure gunicorn is accepting connections
          wait_for:
            port: 5000
            timeout: 60
            delay: 20

        # ADD enable application at lb
        - name: Re-enable application at load balancer
          haproxy:
            backend: catapp-backend
            host: "{{ web_server }}"
            state: enabled
          delegate_to: "{{ item }}"
          loop: "{{ groups.loadbalancer }}"

      rescue:
  ```
  <!-- .element: style="font-size:8pt;"  -->


#### Add _rescue_ behaviour
* Add following to rescue block
  ```
   rescue:
     - name: Output message
       debug:
         msg: "Upgrade to {{ app_version }} failed! Reverting to {{ pre_upgrade_version }}."

     - import_tasks: tasks/setup-app.yml
       vars:
         deploy_version: "{{ pre_upgrade_version }}"   # <-- version originally deployed

     - meta: flush_handlers

     - name: Make sure gunicorn is accepting connections
       wait_for:
         port: 5000
         timeout: 60
         delay: 20

     - name: Re-enable application at load balancer
       haproxy:
         backend: catapp-backend
         host: "{{ web_server }}"
         state: enabled
       delegate_to: "{{ item }}"
       loop: "{{ groups.loadbalancer }}"

     - name: Bail out to avoid deploying to rest of cluster
       fail:
         msg: "Aborting installation of {{ app_version }}!"
  ```
  <!-- .element: style="font-size:8pt;"  -->
* Note we need to pass different version to task


#### Run playbook
* Run the `app-rolling-upgrade` playbook
  - v1
  - v2
  


#### Summary
* It is possible to implement a form of recovery with Ansible
  - block/rescue
* This does not attempt to mitigate issues around DB migration



### The End
* Please do not forget to clean up your clusters!
  ```
  ansible-playbook remove-hosts.yml
  ```
  <!-- .element: style="font-size:11pt;"  -->
